//taken from https://github.com/daveross/flesch-kincaid

var syllables = function syllables(x) {
    /*
     * basic algortithm: each vowel-group indicates a syllable, except for: final
     * (silent) e 'ia' ind two syl @AddSyl and @SubSyl list regexps to massage the
     * basic count. Each match from @AddSyl adds 1 to the basic count, each
     * @SubSyl match -1 Keep in mind that when the regexps are checked, any final
     * 'e' will have been removed, and all '\'' will have been removed.
     */
    var subSyl = [/cial/, /tia/, /cius/, /cious/, /giu/, // belgium!
        /ion/, /iou/, /sia$/, /.ely$/, // absolutely! (but not ely!)
        /sed$/];

    var addSyl = [/ia/, /riet/, /dien/, /iu/, /io/, /ii/, /[aeiouym]bl$/, // -Vble, plus -mble
        /[aeiou]{3}/, // agreeable
        /^mc/, /ism$/, // -isms
        /([^aeiouy])\1l$/, // middle twiddle battle bottle, etc.
        /[^l]lien/, // // alien, salient [1]
        /^coa[dglx]./, // [2]
        /[^gq]ua[^auieo]/, // i think this fixes more than it breaks
        /dnt$/];

    // (comments refer to titan's /usr/dict/words)
    // [1] alien, salient, but not lien or ebbullient...
    // (those are the only 2 exceptions i found, there may be others)
    // [2] exception for 7 words:
    // coadjutor coagulable coagulate coalesce coalescent coalition coaxial

    var xx = x.toLowerCase().replace(/'/g, '').replace(/e\b/g, '');
    var scrugg = xx.split(/[^aeiouy]+/).filter(Boolean); // '-' should be perhaps added?

    return undefined === x || null === x || '' === x ? 0 : 1 === xx.length ? 1 : subSyl.map(function (r) {
        return (xx.match(r) || []).length;
    }).reduce(function (a, b) {
        return a - b;
    }) + addSyl.map(function (r) {
        return (xx.match(r) || []).length;
    }).reduce(function (a, b) {
        return a + b;
    }) + scrugg.length - (scrugg.length > 0 && '' === scrugg[0] ? 1 : 0) +
        // got no vowels? ("the", "crwth")
        xx.split(/\b/).map(function (x) {
            return x.trim();
        }).filter(Boolean).filter(function (x) {
            return !x.match(/[.,'!?]/g);
        }).map(function (x) {
            return x.match(/[aeiouy]/) ? 0 : 1;
        }).reduce(function (a, b) {
            return a + b;
        });
};

var words = function words(x: string): number {
    return (x.split(/\s+/) || ['']).length;
};
var characters = function characters(x: string): number {
    let words = x.split(/\s+/) || [''];
    return words.join("").length;
}
var sentences = function sentences(x: string): number {
    return (x.split('. ') || ['']).length;
};
var syllablesPerWord = function syllablesPerWord(x: string): number {
    //console.log(`words ${words(x)} syllables ${syllables(x)}`);
    return syllables(x) / words(x);
};
var wordsPerSentence = function wordsPerSentence(x: string): number {
    //console.log(`words ${words(x)} sentences ${sentences(x)}`);
    return words(x) / sentences(x);
};

let convert = function (num: number, fixed: number = 2): number {
    return parseFloat(num.toFixed(fixed));
}
//Flesch Kincaid Ease Score (fres)
export function fres(x: string): number {
    let num: number = (206.835 - 1.015 * wordsPerSentence(x) - 84.6 * syllablesPerWord(x));
    return convert(num);
}

export function grade(x: string): number {
    let num = 0.39 * wordsPerSentence(x) + 11.8 * syllablesPerWord(x) - 15.59;
    return convert(num);
}

export function automatedReadabilityIndex(x: string): number {
    let theseWords = words(x),
        chars = characters(x),
        theseSentences = sentences(x);
        //console.log(`${theseWords} ${chars} ${theseSentences}`);
    let num = (4.71 * (chars / theseWords)) + (0.5 * (theseWords / theseSentences)) - 21.43;
    return convert(num, 0);
}


export function ease(x: string): string {
    let num = fres(x),
        lbl = "";
    //dirty hack
    switch (true) {
        case (num <= 30):
            lbl = `Very Difficult`;
            break;
        case (num <= 50):
            lbl = `Difficult`;
            break;
        case (num <= 60):
            lbl = `Fairly Difficult`;
            break;
        case (num <= 70):
            lbl = `Plain English`;
            break;
        case (num <= 80):
            lbl = `Fairly Easy`;
            break;
        case (num <= 90):
            lbl = `Easy`;
            break;
        case (num > 90):
            lbl = `Very Easy`;
            break;
    }
    return `${num} (${lbl})`;
}