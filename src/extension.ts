// The module 'vscode' contains the VS Code extensibility API
// Import the necessary extensibility types to use in your code below
import { window, commands, Disposable, ExtensionContext, StatusBarAlignment, StatusBarItem, TextDocument } from 'vscode';
import { ease, grade, automatedReadabilityIndex } from './flesch-kincaid';

// This method is called when your extension is activated. Activation is
// controlled by the activation events defined in package.json.
export function activate(context: ExtensionContext) {

    // Use the console to output diagnostic information (console.log) and errors (console.error).
    // This line of code will only be executed once when your extension is activated.
    console.log('Congratulations, your extension "Grade" is now active!');

    // create a new word counter
    let wordGrades = new Grades();
    let controller = new GradesController(wordGrades);

    // Add to a list of disposables which are disposed when this extension is deactivated.
    context.subscriptions.push(controller);
    context.subscriptions.push(wordGrades);
}

class Grades {

    private _statusGradeScore: StatusBarItem;

    public updateGrade() {

        // Create as needed
        if (!this._statusGradeScore) {
            this._statusGradeScore = window.createStatusBarItem(StatusBarAlignment.Left);
        }

        // Get the current text editor
        let editor = window.activeTextEditor;
        if (!editor) {
            this._statusGradeScore.hide();
            return;
        }

        let doc = editor.document;
        // Only update status if an Markdown file
        if (doc.languageId === "markdown" || doc.languageId === "plaintext") {
            let wordGrade = this._getFKScores(doc);
            // Update the status bar
            this._statusGradeScore.text = `Flesch Kincaid Grade: ${wordGrade.grade} Ease: ${wordGrade.ease} Readability: ${wordGrade.automatedReadabilityIndex}`;
            this._statusGradeScore.show();
        } else {
            this._statusGradeScore.hide();
        }
    }

    public _getFKScores(doc: TextDocument): any {

        let docContent = doc.getText();
        // Parse out unwanted whitespace so the split is accurate
        docContent = docContent.replace(/(< ([^>]+)<)/g, '').replace(/\s+/g, ' ');
        docContent = docContent.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        return {
            ease: ease(docContent),
            grade: grade(docContent),
            automatedReadabilityIndex: automatedReadabilityIndex(docContent)
        };
    }

    dispose() {
        this._statusGradeScore.dispose();
    }
}

class GradesController {

    private _wordGrades: Grades;
    private _disposable: Disposable;

    constructor(wordGrades: Grades) {
        this._wordGrades = wordGrades;
        this._wordGrades.updateGrade();

        // subscribe to selection change and editor activation events
        let subscriptions: Disposable[] = [];
        window.onDidChangeTextEditorSelection(this._onEvent, this, subscriptions);
        window.onDidChangeActiveTextEditor(this._onEvent, this, subscriptions);

        // update the counter for the current file
        this._wordGrades.updateGrade();

        // create a combined disposable from both event subscriptions
        this._disposable = Disposable.from(...subscriptions);
    }

    dispose() {
        this._disposable.dispose();
    }

    private _onEvent() {
        this._wordGrades.updateGrade();
    }
}